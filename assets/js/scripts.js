(function($){

	/* Slider gallery */
	$('.js-thumblist').on('click','img',function(){
		var thumb = $(this).clone().attr({'width':'','height':''});
		var linked = $("<a />");
		linked.attr('href', thumb.attr('src') ).attr('rel','prettyPhoto');
		linked.append(thumb);
		$('.js-preview').html(linked);

		$("a[rel^='prettyPhoto']").prettyPhoto({
			'social_tools'	: false
		});

	});


	/* TAB NAVIGATION */
	$('.js-tabnav').on('click','button',function(){
		$('.js-tabnav button.is-active').removeClass('is-active');
		$(this).addClass('is-active');
		var  target = $(this).data('target'),
		       scope = $(this).data('scope');

		$(scope+'.is-active').fadeOut(0,function(){
			$(this).removeClass('is-active');
		});

		$(scope+target).fadeIn(600,function(){
			$(this).addClass('is-active');
		});

	});



	/* Instafeed */
/* Instagram carousel */
            var feed = new Instafeed({
	            'get'        : 'user',
	            'userId'     : 1546668473,
	            'clientId'   : ' 7b56929b0b244df7b44bfbd02c2789f4',
	            'accessToken': '1546668473.1677ed0.19030ed45c954c48b904bca2ce2eeaad',
	            'limit'      : 12 ,
	            'template'   : '<li><div><a target="_blank" href="{{link}}"><img width="140" height="140" src="{{image}}" /></a></div></li>'
            });

            feed.run();	


})(jQuery);